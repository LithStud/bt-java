package rimvis.classwork.c05;

import java.math.BigInteger;
import java.util.Scanner;

public class C05 {

    public static void main(String[] args) {
        BigInteger ryziai = BigInteger.ZERO;
        BigInteger diena = BigInteger.ONE;
        for (int i = 1; i <= 64; i++) {
            ryziai = ryziai.add(diena);
            diena = diena.multiply(BigInteger.valueOf(2));
        }

        System.out.println("Viso ryziu: " + ryziai);

        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            System.out.print("Iveskite skaiciu: ");
            long sk = scanner.nextLong();
            if (sk <= 0) break;
            System.out.println(sk + "! = " + factorial(sk));
        }
    }

    static BigInteger factorial(long n) {
        if (n <= 1) return BigInteger.ONE;
        return BigInteger.valueOf(n).multiply(factorial(n - 1));
    }
}
