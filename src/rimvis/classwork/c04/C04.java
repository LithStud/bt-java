package rimvis.classwork.c04;

public class C04 {
    public static void main(String[] args) {
        System.out.println("1..100 Suma: " + sum100(100));
        System.out.println((int)2.6);
    }

    public static int sum100(int n) {
        if (n == 1)
            return 1;
        return n + sum100(n-1);
    }
}
