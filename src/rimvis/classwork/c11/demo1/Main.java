package rimvis.classwork.c11.demo1;

public class Main {

    public static void main(String[] args) {
        Kolekcija<Integer> kolekcija = new Kolekcija<>();

        for (int i = 0; i < 11; i++) {
            kolekcija.add(i);
        }
        System.out.println(kolekcija.toString());

        System.out.println(kolekcija.remove(5));
        System.out.println(kolekcija.toString());
    }
}

class Kolekcija<T> {
    private int BS = 100;
    T[] masyvas;
    private int size = 0;

    public void add(T e) {
        if (size == 0) {
            masyvas = (T[]) new Object[BS];
            masyvas[0] = e;
            size = 1;
            return;
        }

        if (size == masyvas.length) {
            T[] naujas = (T[]) new Object[size + BS];
            System.arraycopy(masyvas, 0, naujas, 0, size);
            masyvas = naujas;
        }

        masyvas[size] = e;
        size++;
    }

    public T remove(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        T e = masyvas[index];
        System.arraycopy(masyvas, index+1, masyvas, index, size-index);
        size--;
        return e;
    }

    public String toString() {
        StringBuilder bs = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (bs.length() > 0) bs.append(",");
            bs.append(masyvas[i]);
        }
        return bs.toString();
    }
}