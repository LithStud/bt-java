package rimvis.homework.p04;

/**
 * 1. Turime du masyvus int[] a = {5, 6, 10, 15, 8, 4} ir int[] b = {8, 5, 3}. Raskite
 * kiekvieno masyvo skaičių vidurkį ir atspausdinkite jų skirtumą. Rezultatas turi
 * gautis: 2.66666…
 * <p>
 * 2. Tobuluoju skaičiumi vadinamas natūralusis skaičius, lygus visų savo daliklių,
 * mažesnių už save patį, sumai.
 * pvz 28 = 1 + 2 + 4 + 7 + 14
 * Suraskite visus tokius skaičius iš intervalo 1…1000.
 * <p>
 * 3. Suraskite duotame intervale visus pirminius skaičius ir juos atspausdinkite
 * <p>
 * 4. Parašykite programą, kuri mokėtų suapvalinti double tipo skaičius pagal
 * nurodytą tikslumą. Panaudokite matematinę funkciją Math.floor(…).
 */
public class P04 {

    public static void main(String[] args) {
        // Užduotis 1
        System.out.println("======[ Užduotis 1 ]======");
        int[] a = {5, 6, 10, 15, 8, 4};
        int[] b = {8, 5, 3};
        System.out.println("a ir b vidurkiu skirtumas: " + (average(a) - average(b)));
        System.out.println("==========================\n");

        // Užduotis 2
        System.out.println("======[ Užduotis 2 ]======");
        for (int i = 1; i <= 1000; i++) {
            if (isPerfect(i))
                System.out.println("Rastas natūralusis skaičius: " + i);
        }
        System.out.println("==========================\n");

        // Užduotis 3
        System.out.println("======[ Užduotis 3 ]======");
        int totalPrimes = 0;
        for (int i = 1; i <= 1000; i++) {
            if (isPrime(i)) {
                System.out.println("Rastas pirminis skaičius: " + i);
                totalPrimes++;
            }
        }
        System.out.println("Iš viso pirminių skaičių: " + totalPrimes);
        System.out.println("==========================\n");

        // Užduotis 4
        System.out.println("======[ Užduotis 4 ]======");
        System.out.println("apvalinam 5.6915 paliekant 1 skaičių po kablelio: " + roundNumber(5.6915, 1));
        System.out.println("==========================\n");
    }

    /**
     * Grazina masyvo elementu vidurki.
     *
     * @param masyvas - sveiku skaičių masyvas
     * @return vidurkis
     */
    static double average(int[] masyvas) {
        int suma = 0;
        for (int i : masyvas)
            suma += i;
        return (double) suma / masyvas.length;
    }

    /**
     * Patikrina ar duotas skaičius yra natūralus
     *
     * @param n - skaičius patirinti
     * @return boolean
     */
    static boolean isPerfect(int n) {
        if (n <= 1)
            return false;
        int sum = 1;
        /* sukti iki skaičiaus šaknies, nes toje vietoje verčiasi įmanomi dalmenys
           pvz.: 16
           1 * 16
           2 * 8
           4 * 4  <- 16 šaknis
           8 * 2
           16 * 1
        */
        for (int i = 2; i <= (int) Math.sqrt(n); i++) {
            if (n % i == 0) {
                sum += i; // dalmuo tinka
                sum += n / i; // skaičius kuris gaunasi padalinus iš rasto dalmens mums irgi tinka
            }
        }
        return sum == n;
    }

    /**
     * Patikrina ar duotas skaičius yra pirminis
     *
     * @param n - skaičius patikrinti
     * @return boolean
     */
    static boolean isPrime(int n) {
        if (n <= 1)
            return false;
        for (int i = 2; i < n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    /**
     * Suapvalina skaičių iki duoto kiekio skaičių po kablelio
     *
     * @param n         - Skaičius suapvalint
     * @param precision - kiek palikti po kablelio
     * @return - suapvalintas skaičius
     */
    static double roundNumber(double n, int precision) {
        double alteredNumber = n * (precision * 10);
        return Math.round(alteredNumber) / (double) (precision * 10);
    }
}
