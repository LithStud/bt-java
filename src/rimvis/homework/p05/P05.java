package rimvis.homework.p05;

import java.math.BigInteger;

/**
 * Surasti labai didelio skaičiaus pirminius skaičius
 * p1 * p2 = X
 * mes turim X reikia rasti p1 ir p2 pirminius skaičius
 */
public class P05 {

    public static void main(String[] args) {
        BigInteger p1 = new BigInteger("195739");
        BigInteger p2 = new BigInteger("104953");
        // BigInteger x = p1.multiply(p2);
        BigInteger x = new BigInteger("-205439529"); // 102719764 -- 16 min (using / 2), kad nerastu | 14333 -- 4s
        BigInteger prime = new BigInteger("2"); // initial prime to start checking from
        BigInteger squareRoot = sqrt(x); // breakpoint to iterate primes untill

        System.out.println("X number: " + x);

        if(x.compareTo(new BigInteger("0")) < 0 ) {
            System.out.println(x + " is invalid input.");
            return;
        }

        // Check if its 2 and another prime is the result (as only another prime multiplied by 2
        // will produce even X
        BigInteger[] checkForEvenPrime = x.divideAndRemainder(prime);
        if (checkForEvenPrime[1].equals(BigInteger.ZERO) && checkForEvenPrime[0].isProbablePrime(1)) {
            System.out.println("Iterated: 1 and Found primes: " + prime + " x " + checkForEvenPrime[0] + " = " + x);
            return;
        } else if (checkForEvenPrime[1].equals(BigInteger.ZERO)) {
            // if its divisible by 2 but second number isnt prime means X is not product of two primes
            System.out.println("X: " + x + " is not a result of two primes");
            return;
        }

        // Start testing other primes
        prime = prime.nextProbablePrime();
        BigInteger[] test; // result of division by currently tested prime number
        BigInteger i = BigInteger.ONE; // counter of iterations
        while (true) {
            test = x.divideAndRemainder(prime);
            if (test[1].equals(BigInteger.ZERO) && test[0].isProbablePrime(1)) {
                System.out.println("Iterated: " + i + " and Found primes: " + prime + " x " + test[0] + " = " + x);
                break;
            }
            prime = prime.nextProbablePrime();
            i = i.add(BigInteger.ONE);
            if (prime.compareTo(squareRoot) > 0) {
                System.out.println("Iterated: " + i + " nothing found. Last prime checked: " + prime);
                break;
            }/* else
                System.out.println(prime);*/
        }
    }

    /**
     * Returns square root of given BigInteger
     * Code taken from https://gist.github.com/JochemKuijpers/cd1ad9ec23d6d90959c549de5892d6cb
     * @param n - Given BigInteger to do square root
     * @return square root BigInteger
     */
    static BigInteger sqrt(BigInteger n) {
        BigInteger a = BigInteger.ONE;
        BigInteger b = n.shiftRight(5).add(BigInteger.valueOf(8));
        while (b.compareTo(a) >= 0) {
            BigInteger mid = a.add(b).shiftRight(1);
            if (mid.multiply(mid).compareTo(n) > 0) {
                b = mid.subtract(BigInteger.ONE);
            } else {
                a = mid.add(BigInteger.ONE);
            }
        }
        return a.subtract(BigInteger.ONE);
    }
}
