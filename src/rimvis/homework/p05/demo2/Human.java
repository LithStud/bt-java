package rimvis.homework.p05.demo2;

public class Human {
    String name;
    String surname;

    public Human(String name) {
        this.name = name;
    }

    public Human(String name, String surname) {
        this(name);
        this.surname = surname;
    }

    public String getFullName() {
        return this.name + " " + this.surname;
    }

    /**
     * Compares two humans using surname then name for decision
     * @param human - another human to compare to
     * @return -1 - lower, 0 - equils, 1 - higher
     */
    public int compareTo(Human human) {
        if (this.surname.compareTo(human.surname) > 0)
            return 1;
        else if (this.surname.compareTo(human.surname) < 0)
            return -1;

        // same surname
        return this.name.compareTo(human.name);
    }
}
