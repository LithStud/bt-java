package rimvis.homework.p05.demo2;

public class Student extends Human {
    int classNumber;

    public Student(String name, String surname, int classNumber) {
        super(name, surname);
        this.classNumber = classNumber;
    }

    public String getFullInfo() {
        return this.getFullName() + " " + this.classNumber;
    }

    /**
     * Compares two studends first class then surname then name
     * @param student - student to compare with
     * @return -1 - lower, 0 - equils, 1 - higher
     */
    public int compareTo(Student student) {
        if (this.classNumber > student.classNumber)
            return 1;
        else if (this.classNumber < student.classNumber)
            return -1;

        // same class (use surname, name check)
        return super.compareTo(student);
    }
}
