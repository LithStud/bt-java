package rimvis.homework.p05.demo2;

/*
Tarkime turime masyvą objektų Student.
Reikia atspausdinti mokinių vardus ir pavardes surūšiuotus pagal klases ir pagal pavardes bei vardus.
 */
public class Main {

    public static void main(String[] args) {
        School school = new School(new Student[]{
                new Student("Petras", "Laukys", 5),
                new Student("Jonas", "Laukys", 5),
                new Student("Autis", "Kapys", 3),
                new Student("Ona", "Laukute", 5),
                new Student("Saule", "Apalyte", 12),
                new Student("Kedras", "Azuolas", 3),
                new Student("Marius", "Sliumpa", 5),
                new Student("Giedrius", "Simkus", 11),
        });

        System.out.println("======[ Unsorted ]======");
        school.printStudents();
        System.out.println("========================");

        school.sortStudents();
        System.out.println("======[  Sorted  ]======");
        school.printStudents();
        System.out.println("========================");
    }
}
