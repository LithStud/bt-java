package rimvis.homework.p05.demo1;

public class main {

    public static void main(String[] args) {
        Zmogus[] zmones = {
                new Zmogus("Vardis"),
                new Zmogus("Ravis"),
                new Zmogus("Tretys")
        };

        for (Zmogus zmogus: zmones) {
            System.out.println(zmogus.name);
        }
    }
}
