package rimvis.homework.p03;

import java.util.Arrays;

/**
 * Class 03 - Variables
 * 1. Duoti trys skaičiai: a, b, c. Nustatykite ar šie skaičiai gali būti trikampio kraštinių
 * ilgiai ir jei gali tai kokio trikampio: lygiakraščio, lygiašonio ar įvairiakraščio.
 * Atspausdinkite atsakymą. Kaip pradinius duomenis panaudokite tokius skaičius:
 * 3, 4, 5
 * 2, 10, 8
 * 5, 6, 5
 * 5, 5, 5
 * <p>
 * 2. Apskaičiuokite ir atspausdinkite šių trikampių plotus
 * Spausdinimui naudokite: System.out.println("Plotas=" + plotas);
 * <p>
 * 3. Duotas masyvas {-10, 0, 2, 9, -5}. Surūšiuokit masyvo elementus mažėjimo
 * tvarka ir atspausdinkite.
 * ✴Nenaudokit standartinės masyvo rūšiavimo funkcijos :)
 */
public class P03 {
    /**
     * Main method
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        int a,b,c;
        // Užduoties 1, 2 dalys
        // paduota trys kraštinės
        if (args.length == 3) {
            a = Integer.parseInt(args[0]);
            b = Integer.parseInt(args[1]);
            c = Integer.parseInt(args[2]);
            System.out.println("=======[ Trikampis manual ]=======");
            System.out.println("Kraštinės: " + Arrays.toString(args));
            if (isValid(a, b, c)) {
                System.out.println("Legalus trikampis");
                System.out.println("Tipas: " + printType(a, b, c));
                System.out.println("Plotas: " + area(a, b, c));
            } else {
                System.out.println("Nelegalus trikampis");
            }
            System.out.println("=============================\n");
        } else {
            int[][] trikampiai = {{3, 4, 5}, {2, 10, 8}, {5, 6, 5}, {5, 5, 5}};
            for (int i = 0; i < trikampiai.length; i++) {
                a = trikampiai[i][0];
                b = trikampiai[i][1];
                c = trikampiai[i][2];
                System.out.println("=======[ Trikampis " + i + " ]=======");
                System.out.println("Kraštinės: " + Arrays.toString(trikampiai[i]));
                if (isValid(a, b, c)) {
                    System.out.println("Legalus trikampis");
                    System.out.println("Tipas: " + printType(a, b, c));
                    System.out.println("Plotas: " + area(a, b, c));
                } else {
                    System.out.println("Nelegalus trikampis");
                }
                System.out.println("=============================\n");
            }
        }


        // Užduoties 3 dalis masyvo rūšiavimas
        System.out.println("-=====[ Array Sorting ]=====-");
        int[] masyvas = {-10, 0, 2, 9, -5, -10, 0, 2, 9, -5};
        System.out.println("Unsorted: " + Arrays.toString(masyvas));
        sort(masyvas);
        System.out.println("Sorted: " + Arrays.toString(masyvas));
        System.out.println("-===========================-\n");
    }

    /**
     * Checks if triangle is valid triangle returns true if not valid
     *
     * @param a - Side A
     * @param b - Side B
     * @param c - Side C
     * @return boolean
     */
    private static boolean isValid(int a, int b, int c) {
        return (a + b > c && a + c > b && b + c > a);
    }

    /**
     * Checks what type of triangle it is. Returns type String.
     *
     * @param a - Side A
     * @param b - Side B
     * @param c - Side C
     * @return String
     */
    private static String printType(int a, int b, int c) {
        if (a == b && a == c)
            return "Lygiakraštis";
        if (a == b || a == c || b == c)
            return "Lygiašonis";

        return "Įvairiakraštis";
    }

    /**
     * Calculates are of triangle
     *
     * @param a - Side A
     * @param b - Side B
     * @param c - Side C
     * @return double
     */
    private static double area(double a, double b, double c) {
        double p = (a + b + c) / 2; // pusperimetris
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    /**
     * Sorts supplied array
     *
     * @param unsorted pointer to array of integers
     */
    private static void sort(int[] unsorted) {
        int cal = 0;
        for (int i = 0; i < unsorted.length - 1; i++) {
            for (int j = i + 1; j < unsorted.length; j++) {
                if (unsorted[i] < unsorted[j]) {
                    int temp = unsorted[i];
                    unsorted[i] = unsorted[j];
                    unsorted[j] = temp;
                }
                cal++;
            }
        }
        System.out.println("Prasuko: " + cal);
    }
}
