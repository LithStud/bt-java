package rimvis.homework.p11.task1;

import java.util.*;

/**
 * Sukurkite žodyno tipo kolekciją saugoti žmonių klasės objektus (su tokiais laukais:
 * vardas, pavardė, asmens kodas), o kaip raktą naudokite asmens kodą.
 * Įdėkite keletą žmonių į kolekciją ir atspausdinkite žmones asmens kodo didėjimo
 * tvarka.
 * Pabandykite įdėti į kolekciją du skirtingus žmones bet su tuo pačiu asmens kodu.
 * Patikrinkite kas atsitiks?
 * Pagalvokite kaip saugoti žmones jei norime turėti kelis su tuo pačiu asmens kodu.
 */
public class Main {

    static TreeMap<String, Human> humans;
    static TreeMap<String, HumanList> humans2;

    public static void main(String[] args) {
        Human petras = new Human("Petras", "Petraitis", "3196609160619");
        Human ona = new Human("Ona", "Onaite", "4196709160619");
        Human jonas = new Human("Jonas", "Petraitis", "3196609160619");

        // First Part
        System.out.println("\n******* PART 1 *******\n");
        humans = new TreeMap<>();

        addHuman(ona);
        addHuman(petras);
        addHuman(jonas);

        printMap(humans);

        // Part 2
        System.out.println("\n******* PART 2 *******\n");
        humans2 = new TreeMap<>();

        addHuman2(ona);
        addHuman2(petras);
        addHuman2(jonas);

        printMap2(humans2);
    }

    /**
     * Prints simple TreeMap with Human objects
     *
     * @param map - TreeMap
     */
    public static void printMap(TreeMap<String, Human> map) {
        for (Map.Entry<String, Human> item : map.entrySet()) {
            System.out.println(item.getKey() + " " + item.getValue().getName() + " " + item.getValue().getSurname());
        }
    }

    /**
     * Prints TreeMap pf HumanList objects that manages Human objects
     *
     * @param map - TreeMap
     */
    public static void printMap2(TreeMap<String, HumanList> map) {
        for (Map.Entry<String, HumanList> item : map.entrySet()) {
            System.out.println("\n" + item.getValue().toString());
        }
    }

    /**
     * Adds Human object to simple TreeMap
     *
     * @param human - Human object
     */
    static void addHuman(Human human) {
        Human inserter = null;
        System.out.println("Add " + human.getName() + " with key: " + human.getId());
        inserter = humans.put(human.getId(), human);
        System.out.println(inserter == null ? "Added" : "Replaced " + inserter.getName());
    }

    /**
     * Adds Human object to HumanList in TreeMap. is key exists Human is added to existing HumanList.
     *
     * @param human - Human Object
     */
    static void addHuman2(Human human) {
        System.out.println("Add " + human.getName() + " with key: " + human.getId());
        HumanList inserter = humans2.get(human.getId());
        if (inserter == null) {
            System.out.println("New Entry");
            humans2.put(human.getId(), new HumanList(human));
        } else {
            System.out.println("ID Exists, adding to list");
            inserter.addHuman(human);
        }
    }
}

/**
 * Manages Humans with same IDs ( Human objects stored in ArrayList)
 */
class HumanList {
    private ArrayList<Human> humans;

    public HumanList(Human human) {
        humans = new ArrayList<>();
        humans.add(human);
    }

    /**
     * Adds Human to ArrayList
     *
     * @param human - Human Object
     */
    public void addHuman(Human human) {
        humans.add(human);
    }

    /**
     * Deletes Human from ListArray
     *
     * @param human
     * @return
     */
    public Human removeHuman(Human human) {
        int index = humans.indexOf(human);
        if (index > 0) return humans.remove(index);
        return null;
    }

    /**
     * Returns nicely formated ListArray of Humans
     *
     * @return
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Human human : humans) {
            if (sb.length() == 0) sb.append("-===[ " + human.getId() + " ]===-\n");
            sb.append(human.getName() + " " + human.getSurname() + "\n");
        }
        if (sb.length() > 0) sb.append("=========================");
        return sb.toString();
    }
}
