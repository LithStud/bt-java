package rimvis.homework.p09.task1;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // Task 1 simple info getters
        Payment[] mokejimai = {
                new Employee("LT00", "Petras", 10),
                new Employee("LT01", "Jonas", 10),
                new Employee("LT02", "Ona", 10),
                new Client("LT03", "Ranimta"),
                new Client("LT04", "Kuolas"),
                new Client("LT05", "Vartai"),
        };

        for (Payment mokejimas : mokejimai) {
            System.out.println(mokejimas.getOwner() + "[ " + mokejimas.getBankAccount() + " ]");
        }

        // Task 2 finding suspicious transactions
        Client demo = new Client("LT55", "DEmo");
        demo.pay(mokejimai[0], 12);
        demo.pay(mokejimai[0], 24);
        demo.pay(mokejimai[2], 12);
        //demo.printTxs();
        System.out.println("Average: " + demo.getAverage());

        Client demo2 = new Client("LT66", "Emo");
        demo2.pay(mokejimai[0], 25);
        demo2.pay(mokejimai[1], 25);
        demo2.pay(mokejimai[2], 25);
        demo2.pay(mokejimai[2], 250);

        System.out.println("Average: " + demo2.getAverage());

        int totalTx = 0;
        double totalSent = 0;
        totalSent += demo.getTotal() + demo2.getTotal();
        totalTx += demo.getTxAmount() + demo2.getTxAmount();

        System.out.println("Total Avg.: " + totalSent / totalTx);

        demo.printSuspicious(totalSent / totalTx);
        demo2.printSuspicious(totalSent / totalTx);
    }
}
