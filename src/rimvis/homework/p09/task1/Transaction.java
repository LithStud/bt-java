package rimvis.homework.p09.task1;

public class Transaction implements Payment {
    private Payment to;
    private double amount = 0;

    public Transaction(Payment to, double amount) {
        this.to = to;
        this.amount = amount;
    }

    @Override
    public String getBankAccount() {
        return to.getBankAccount();
    }

    @Override
    public String getOwner() {
        return to.getOwner();
    }

    @Override
    public double getAmount() {
        return this.amount;
    }

    /*
        NOT NEEDED FOR TRANSACTION SCOPE
     */
    @Override
    public void printTxs() {}

    @Override
    public void pay(Payment to, double amount) {}
}
