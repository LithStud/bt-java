package rimvis.homework.p09.task1;

import java.util.ArrayList;
import java.util.Arrays;

public class Client implements Payment {
    private String bankAccount;
    private String name;
    private ArrayList<Payment> txs;

    public Client(String bankAccount, String name) {
        this.bankAccount = bankAccount;
        this.name = name;
        this.txs = new ArrayList<Payment>();
    }

    @Override
    public String getBankAccount() {
        return this.bankAccount;
    }

    @Override
    public String getOwner() {
        return "Imone: " + this.name;
    }

    @Override
    public double getAmount() {
        return 0;
    }

    @Override
    public void pay(Payment to, double amount) {
        this.txs.add(new Transaction(to, amount));
        System.out.println(this.getOwner() + " payed " + amount + " to " + to.getOwner());
    }

    @Override
    public void printTxs() {
        for(Payment transaction : this.txs) {
            System.out.println(transaction.getAmount());
        }
    }

    public double getAverage() {
        double sum = 0;
        for(Payment transaction : this.txs) {
            sum += transaction.getAmount();
        }
        return sum / this.txs.size();
    }

    public double getTotal() {
        double sum = 0;
        for(Payment transaction : this.txs) {
            sum += transaction.getAmount();
        }
        return sum;
    }

    public int getTxAmount() {
        return this.txs.size();
    }

    public void printSuspicious(double avg) {
        double limit = avg * 2;
        int totalSuspiciousTx = 0;
        for (Payment tx: this.txs) {
            if (tx.getAmount() >= limit) {
                System.out.println(this.getOwner() + " ] suspicious payment to " +
                        tx.getOwner() + " of " + tx.getAmount());
                totalSuspiciousTx++;
            }
        }
        if (totalSuspiciousTx > 0) {
            System.out.println("Total suspicious transactions: " + totalSuspiciousTx);
        } else {
            System.out.println(getOwner() + " has no suspicious transactions");
        }
    }
}
