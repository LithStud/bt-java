package rimvis.homework.p09.task1;

public class Employee implements Payment {
    private String bankAccount;
    private String name;
    private double money;

    public Employee(String bankAccount, String name, double money) {
        this.bankAccount = bankAccount;
        this.name = name;
        this.money = money;
    }

    @Override
    public String getBankAccount() {
        return this.bankAccount;
    }

    @Override
    public String getOwner() {
        return "Darbuotojas: " + this.name;
    }

    @Override
    public double getAmount() {
        return this.money;
    }

    @Override
    public void printTxs() {
        System.out.println(this.money);
    }

    @Override
    public void pay(Payment to, double amount) {
        System.out.println(this.getOwner() + " payed " + amount + " to " + to.getOwner());
    }

}
