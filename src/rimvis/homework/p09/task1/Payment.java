package rimvis.homework.p09.task1;

/**
 * banko sąskaita (bank account) - grąžina banko sąskaitos numerį
 * sąskaitos turėtojas (account owner) - fizinio ar juridinio asmens pavadinimas
 * suma (amount) - pervedama suma
 */
public interface Payment {
    String getBankAccount();
    String getOwner();
    double getAmount();
    void printTxs();
    void pay(Payment to, double amount);
}
