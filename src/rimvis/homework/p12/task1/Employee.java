package rimvis.homework.p12.task1;

public class Employee {

    String name;
    Address address;

    public Employee(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    static class Address {
        String city;
        String street;
        String number;

        public Address(String city, String street, String number) {
            this.city = city;
            this.street = street;
            this.number = number;
        }
    }
}
