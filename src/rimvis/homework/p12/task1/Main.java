package rimvis.homework.p12.task1;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Employee> db = new ArrayList<>();

        db.add(new Employee("Petras", new Employee.Address("Kaunas", "Suokos g.", "21")));
        db.add(new Employee("Jonas", new Employee.Address("Vilnius", "Kauno g.", "22")));
        db.add(new Employee("Ona", new Employee.Address("Kaunas", "Suokos g.", "23")));
        db.add(new Employee("Marija", new Employee.Address("Klaipeda", "Lopetos g.", "24")));
        db.add(new Employee("Ona", new Employee.Address("Vilnius", "Lopetos g.", "25")));

        Map<String, Employee.Address> cities = new HashMap<>();
        for(int i=0; i < db.size(); i++) {
            Employee e = db.get(i);
            cities.put(e.address.city, e.address);
        }

        System.out.println("Skirtingu miestu: " + cities.size());
        cities.forEach((key, value) -> {
            System.out.println(value.city);
        });
        System.out.println("=============================");


        Set<String> cityAndStreet = new HashSet<>();
        for (Employee e : db) {
            if (!cityAndStreet.contains(e.address.city) && !cityAndStreet.contains(e.address.street)) {
                cityAndStreet.add(e.address.city);
                cityAndStreet.add(e.address.street);
            }
        }
        System.out.println("Skirtingu miestu ir gatviu: " + cityAndStreet.size());

        for(String cs: cityAndStreet) {
            System.out.println(cs);
        }

        System.out.println("=============================");
    }
}
