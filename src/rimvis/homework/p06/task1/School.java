package rimvis.homework.p06.task1;

public class School {
    private Student[] school;

    public School(Student[] school) {
        this.school = school;
    }

    public void printSchool() {
        System.out.println("[ Class ] - Average | Surname Name");
        for (Student stud : school) {
            System.out.println(stud.getStudentInfo());
        }
    }

    public void sortSchool() {
        quickSort(0, school.length - 1);
    }

    /**
     * QuickSort algo
     *
     * @param lo - first index
     * @param hi - last index
     */
    private void quickSort(int lo, int hi) {
        if (lo < hi) {
            int p = partition(lo, hi);
            this.quickSort(lo, p);
            this.quickSort(p + 1, hi);
        }
    }

    /**
     * Sorting partition
     *
     * @param lo - first index
     * @param hi - last index
     * @return partition index
     */
    private int partition(int lo, int hi) {
        Student pivot = this.school[(lo + hi) / 2];
        int i = lo - 1;
        int j = hi + 1;
        while (true) {
            do {
                i++;
            } while (this.school[i].compareTo(pivot) < 0);
            do {
                j--;
            } while (this.school[j].compareTo(pivot) > 0);

            if (i >= j)
                return j;

            this.swapStudents(i, j);
        }
    }

    /**
     * Swaps studends in school array
     *
     * @param i - index
     * @param j - index
     */
    private void swapStudents(int i, int j) {
        Student temp = this.school[i];
        this.school[i] = this.school[j];
        this.school[j] = temp;
    }
}
