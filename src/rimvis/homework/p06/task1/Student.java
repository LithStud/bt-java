package rimvis.homework.p06.task1;

public class Student {
    private int classNumber;
    private String name;
    private String surname;
    private int[] trimester;
    private double average;

    Student(String name, String surname, int classNumber) {
        this.name = name;
        this.surname = surname;
        this.classNumber = classNumber;
    }

    Student(String name, String surname, int classNumber, int... trimesterMarks) {
        this(name, surname, classNumber);
        this.trimester = trimesterMarks;
        calculateAverage();
    }

    private void calculateAverage() {
        int sum = 0;
        for (int mark : trimester)
            sum += mark;

        average = (double) sum / trimester.length;
    }

    /**
     * Compares two studends first class then surname then name
     *
     * @param student - student to compare with
     * @return -1 - lower, 0 - equils, 1 - higher
     */
    public int compareTo(Student student) {
        if (classNumber > student.classNumber)
            return 1;
        else if (classNumber < student.classNumber)
            return -1;
        // same class, compare trimester average
        if (average < student.average)
            return 1;
        else if (average > student.average)
            return -1;

        // Students identical
        return 0;
    }

    /*
     SETTERS & GETTERS
     */

    // CUSTOM

    public String getStudentInfo() {
        return String.format("[%4d ] - ", classNumber) +
                String.format("%.2f | ", average) +
                surname + " " + name;
    }

    public void setTrimester(int... trimester) {
        this.trimester = trimester;
        calculateAverage();
    }

    // AUTO-GENERATED

    public int getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(int classNumber) {
        this.classNumber = classNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getTrimester() {
        return trimester;
    }

    public double getAverage() {
        return average;
    }
}
