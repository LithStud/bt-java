package rimvis.homework.p06.task1;

public class Main {

    public static void main(String[] args) {

        School school = new School(new Student[]{
                new Student("Petras", "Laukys", 5, 5, 10, 5, 6, 9, 5, 4, 1),
                new Student("Jonas", "Laukys", 5, 5, 10, 5, 5, 5, 5, 4, 1),
                new Student("Klonas", "Averyga", 5, 5, 10, 5, 5, 5, 5, 4, 1),
                new Student("Autis", "Kapys", 3, 2, 1, 2, 6, 2, 5, 3, 1),
                new Student("Ona", "Laukute", 5, 5, 10, 5, 4, 9, 5, 4, 1),
                new Student("Saule", "Apalyte", 12, 5, 1, 5, 6, 9, 5, 4, 1),
                new Student("Kedras", "Azuolas", 3, 5, 10, 6, 9, 5, 4, 1),
                new Student("Marius", "Sliumpa", 5, 5, 1, 5, 6, 5, 4, 1),
                new Student("Giedrius", "Simkus", 11, 5, 10, 5, 10, 9, 4, 4, 1)
        });

        System.out.println("======[ Unsorted ]======");
        school.printSchool();
        System.out.println("========================");

        school.sortSchool();
        System.out.println("======[  Sorted  ]======");
        school.printSchool();
        System.out.println("========================");
    }
}
