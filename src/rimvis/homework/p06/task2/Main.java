package rimvis.homework.p06.task2;

public class Main {
    public static void main(String[] args) {
        int people = 13;
        int brackets = 0;
        int totalDays = 0;
        for (int i = 2; i <= people; i *= 2) {
            brackets = i;
            totalDays++;
        }

        Tournament tour = new Tournament(people);
        System.out.println("\nTotal days for main tournament: " + totalDays);
        System.out.println("Brackets in main tournament:" + brackets);

        tour.printMatches();

    }
}
