package rimvis.homework.p06.task2;

class Match {
    private String competitor; // competitor name
    private int level; // tournament level
    private String matchID; // unique node ID (displayed when winner hasnt been determined yet)

    // if its a match it will have 2 nodes (players)
    private Match left;
    private Match right;

    Match(String competitor, int level) {
        this.competitor = competitor;
        this.level = level;
    }

    Match(String competitor, int level, int id) {
        this(competitor, level);
        this.matchID = "ID-" + id;
    }

    Match(int competitor, int level) {
        this(String.valueOf(competitor), level);
    }

    Match(int competitor, int level, int id) {
        this(String.valueOf(competitor), level, id);
    }


    /*
    GETTERS & SETTERS
     */
    public String getCompetitor() {
        return competitor;
    }

    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    public Match getLeft() {
        return left;
    }

    public void setLeft(Match left) {
        this.left = left;
    }

    public Match getRight() {
        return right;
    }

    public void setRight(Match right) {
        this.right = right;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(int matchID) {
        this.matchID = "ID-" + matchID;
    }
}

public class Tournament {

    Match root; // root level (champion)
    int maxLevels; // max number of levels (depth)

    public Tournament(int people) {
        if (people < 2) {
            System.out.println("Tournament with less than 2 people is imposible");
            return;
        }
        // calculate initial values like required max brackets and how deep it will go
        int brackets = 0;
        int depth = 0;
        int seeds = 0;
        for (int i = 2; i <= people; i *= 2) {
            brackets = i;
            depth++;
        }

        root = new Match(null, 0, 0);
        int[] counter = {1}; // just a counter to have unique IDs
        buildTournament(root, depth, 1, counter);
        maxLevels = depth; // never take into count preliminary level

        // if there is people not fiting in brackets means need additional preliminaries round
        seeds = brackets - (people - brackets);

        // add competitors
        for (int i = 1; i <= people; i++) {
            Match emptyNode = firstEmptyLeaf(root);
            if (seeds > 0) {
                emptyNode.setCompetitor(String.valueOf(i));
                seeds--;
            } else {
                emptyNode.setLeft(new Match(i, depth + 1, counter[0]++));
                i++;
                emptyNode.setRight(new Match(i, depth + 1, counter[0]++));
            }
        }

        //printTree(root);
    }

    /**
     * Builds Tournament tree
     *
     * @param rootNode root node for current operation
     * @param depth    current tree depth
     * @param level    current tournament level
     * @param counter  counter for unique match ID
     */
    public void buildTournament(Match rootNode, int depth, int level, int[] counter) {
        if (depth <= 0) return;
        rootNode.setLeft(new Match(null, level, counter[0]));
        counter[0]++;
        rootNode.setRight(new Match(null, level, counter[0]));
        counter[0]++;

        buildTournament(rootNode.getLeft(), depth - 1, level + 1, counter);
        buildTournament(rootNode.getRight(), depth - 1, level + 1, counter);
    }

    /**
     * Prints tree in one line from supplied node
     *
     * @param node root node to start iterating from
     */
    public void printTree(Match node) {
        if (node == null) return;
        System.out.print(" | " + node.getLevel() + "(" + node.getCompetitor() + " <> " + node.getMatchID() + ")" + " | ");
        printTree(node.getLeft());
        printTree(node.getRight());
    }

    /**
     * Finds first empty node without left or right
     *
     * @param node node to check
     * @return pointer to node;
     */
    public Match firstEmptyLeaf(Match node) {
        if (node == null) return null;

        if (node.getCompetitor() == null && node.getRight() == null && node.getLeft() == null)
            return node; // empty final node (cant go deeper)

        // checking goeas from left to right
        Match left = firstEmptyLeaf(node.getLeft());
        if (left != null)
            return left;

        Match right = firstEmptyLeaf(node.getRight());
        if (right != null)
            return right;

        // in case tournament tree is dmaged and there is incomplete nodes
        return null;
    }

    /**
     * find and print matches for set level
     *
     * @param node  node to start searching from
     * @param level level to print
     */
    public void findMatches(Match node, int level) {
        if (node == null) return;
        if (node.getLevel() > level) return;

        if (node.getCompetitor() == null && node.getLeft() != null && node.getRight() != null) {
            if (node.getLevel() == level) {
                System.out.println("Match at level [ " + node.getLevel() + " ] " +
                        (node.getLeft().getCompetitor() == null ?
                                node.getLeft().getMatchID() :
                                node.getLeft().getCompetitor()) +
                        " Vs. " +
                        (node.getRight().getCompetitor() == null ?
                                node.getRight().getMatchID() :
                                node.getRight().getCompetitor()) +
                        " | [ " + node.getMatchID() + " ]");
                return;
            }
        }
        if (node.getLeft() != null)
            findMatches(node.getLeft(), level);
        if (node.getRight() != null)
            findMatches(node.getRight(), level);
    }

    /**
     * prints all tournament matches by level
     */
    public void printMatches() {
        if (root == null)
            System.out.println("No tournament table");
        for (int level = maxLevels; level >= 0; level--) {
            findMatches(root, level);
        }
    }
}
