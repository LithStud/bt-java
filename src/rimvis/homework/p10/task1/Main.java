package rimvis.homework.p10.task1;

import java.text.DecimalFormat;

public class Main {

    final static double PVM = 1.21;
    private static DecimalFormat df2 = new DecimalFormat("#0.00");
    private static DecimalFormat df3 = new DecimalFormat("#0.00#");

    public static void main(String[] args) {
        print(2, 0.78);
        print(25, 999);
    }

    static void print(int amount, double totalWithPVM) {
        double unitPriceWithPVM = totalWithPVM / amount;
        double unitPriceNoPVM = unitPriceWithPVM / PVM;
        double unitPVM = unitPriceWithPVM - unitPriceNoPVM;

        System.out.println(amount + " x " + df3.format(unitPriceNoPVM) + " = " +
                df2.format(unitPriceNoPVM * amount) + " be PVM");
        System.out.println("Viso PVM (21%): " + df2.format(unitPVM * amount));
        System.out.println("Viso su PVM: " + df2.format(totalWithPVM));
    }
}
