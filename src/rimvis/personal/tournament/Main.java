package rimvis.personal.tournament;

public class Main {

    public static void main(String[] args) {
        int people = 16;
        Match[] firstRound = new Match[people / 2];
        for (int i = 0; i < firstRound.length; i++) {
            firstRound[i] = new Match(i, String.valueOf(i * 2 + 1), String.valueOf(i * 2 + 2));
        }

        for (Match match : firstRound) {
            System.out.println("[" + match.getStringId() + "] " + match.getPlayerOne() + " Vs. " + match.getPlayerTwo());
        }

        tournament(firstRound);
    }

    public static void tournament(Match[] arr) {
        if (arr.length == 1) {
            System.out.println("\nFinal: " + arr[0].getPlayerOne() + " Vs. " + arr[0].getPlayerTwo());
            return;
        }

        int brackets = arr.length / 2;
        int id = arr[arr.length - 1].getId() + 1;
        Match[] round = new Match[brackets];

        for (int i = 0; i < brackets; i++) {
            round[i] = new Match(id++, arr[i * 2].getStringId(), arr[i * 2 + 1].getStringId());
        }

        if (round.length > 1) {
            System.out.println("\nNaujas Ratas");
            for (Match match : round) {
                System.out.println("[" + match.getStringId() + "] " + match.getPlayerOne() + " Vs. " + match.getPlayerTwo());
            }
        }
        tournament(round);

    }
}
