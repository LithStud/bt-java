package rimvis.personal.battleship;

import java.util.List;
import java.util.Random;

public class Board {
    private List<Ship> ships;
    private boolean debug = false; // true will enable '+' as markers for bordering tiles near ship
    private final int cols = 10;
    private final int rows = 10;
    private final Cell[][] board = new Cell[rows][cols];

    public Board() {
        insertShip(5);
        insertShip(4);
        insertShip(3);
        insertShip(3);
        insertShip(2);
        insertShip(1);
        insertShip(1);
        insertShip(1);
    }

    public void insertShip(int size) {
        boolean inserted = false;
        while (!inserted) {
            inserted = addShip(size);
        }
    }

    /**
     * adds ship to board according to battleship rules
     * @param size Ship size
     * @return
     */
    public boolean addShip(int size) {
        Random random = new Random();
        int col = random.nextInt(cols);
        int row = random.nextInt(rows);
        boolean vertical = random.nextBoolean();

        if (canFit(row, col, size, vertical)) {
            for (int i = 0; i < size; i++) {
                if (vertical) {
                    board[row + i][col] = new Cell();
                    board[row + i][col].setShip(true);

                    if (i == 0) {
                        if ((row - 1) >= 0) {
                            board[row - 1][col] = new Cell();
                            board[row - 1][col].setBorder(true);
                            if (col - 1 >= 0) {
                                board[row - 1][col - 1] = new Cell();
                                board[row - 1][col - 1].setBorder(true);
                            }
                            if (col + 1 < cols) {
                                board[row - 1][col + 1] = new Cell();
                                board[row - 1][col + 1].setBorder(true);
                            }
                        }
                    }

                    if (col - 1 >= 0) {
                        board[row + i][col - 1] = new Cell();
                        board[row + i][col - 1].setBorder(true);
                    }

                    if (col + 1 < cols) {
                        board[row + i][col + 1] = new Cell();
                        board[row + i][col + 1].setBorder(true);
                    }

                    if (i == size - 1) {
                        if (row + i + 1 < rows) {
                            board[row + i + 1][col] = new Cell();
                            board[row + i + 1][col].setBorder(true);
                            if (col - 1 >= 0) {
                                board[row + i + 1][col - 1] = new Cell();
                                board[row + i + 1][col - 1].setBorder(true);
                            }

                            if (col + 1 < cols) {
                                board[row + i + 1][col + 1] = new Cell();
                                board[row + i + 1][col + 1].setBorder(true);
                            }
                        }
                    }
                } else {
                    board[row][col + i] = new Cell();
                    board[row][col + i].setShip(true);
                    if (i == 0) {
                        if (col - 1 >= 0) {
                            board[row][col - 1] = new Cell();
                            board[row][col - 1].setBorder(true);
                            if (row - 1 >= 0) {
                                board[row - 1][col - 1] = new Cell();
                                board[row - 1][col - 1].setBorder(true);
                            }
                            if (row + 1 < rows) {
                                board[row + 1][col - 1] = new Cell();
                                board[row + 1][col - 1].setBorder(true);
                            }
                        }
                    }

                    if (row - 1 >= 0) {
                        board[row - 1][col + i] = new Cell();
                        board[row - 1][col + i].setBorder(true);
                    }

                    if (row + 1 < rows) {
                        board[row + 1][col + i] = new Cell();
                        board[row + 1][col + i].setBorder(true);
                    }

                    if (i == size - 1) {
                        if (col + i + 1 < cols) {
                            board[row][col + i + 1] = new Cell();
                            board[row][col + i + 1].setBorder(true);

                            if (row - 1 >= 0) {
                                board[row - 1][col + i + 1] = new Cell();
                                board[row - 1][col + i + 1].setBorder(true);
                            }

                            if (row + 1 < rows) {
                                board[row + 1][col + i + 1] = new Cell();
                                board[row + 1][col + i + 1].setBorder(true);
                            }
                        }
                    }
                }
            }
            return true;
        } else {
            System.out.println("Ship of size " + size + " didnt fit into board");
            return false;
        }

        //if (canFit(row, col, size, vertical))
        //    System.out.println("["+row+","+col+"] - Size: "+size+" FITS " + (vertical ? "verticaly" : "horizontaly"));
        //else System.out.println("["+row+","+col+"] - Size: "+size+" DOESNT FIT " + (vertical ? "verticaly" : "horizontaly") );
    }

    /**
     * Checks if given size and orientation ship can fit on the board from given coordinates.
     * @param row Starting row
     * @param col Starting column
     * @param size ship size (2..5)
     * @param vertical false - horizontal, true - vertical
     * @return
     */
    private boolean canFit(int row, int col, int size, boolean vertical) {
        if (row < 0 || col < 0) return false;
        if (vertical) {
            if (row + size >= rows) return false;
            for (int i = 0; i < size; i++) {
                if (board[row + i][col] != null) return false;
            }
            return true;
        } else {
            if (col + size >= cols) return false;
            for (int i = 0; i < size; i++) {
                if (board[row][col + i] != null) return false;
            }
            return true;
        }
    }

    /**
     * Prints board state into terminal
     */
    public void print() {
        StringBuilder sb = new StringBuilder();
        sb.append("  ");
        for (XCoord x : XCoord.values()) {
            sb.append("  " + x.name() + "  ");
        }
        sb.append("\n");
        for (int row = 0; row < rows; row++) {
            sb.append(row + " ");
            for (int col = 0; col < cols; col++) {
                if (board[row][col] == null)
                    sb.append(" [ ] ");
                else if (board[row][col].isShip()) sb.append(" [o] ");
                else if (debug && board[row][col].isBorder()) sb.append(" [+] ");
                else sb.append(" [ ] ");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

}
