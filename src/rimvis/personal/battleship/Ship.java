package rimvis.personal.battleship;

import java.util.ArrayList;
import java.util.List;

public class Ship {

    private List<ShipPart> shipBlocks;

    public Ship(int size, boolean vertical) {
        if (size < 2 || size > 5) return;
        shipBlocks = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            if (vertical) {
                shipBlocks.add(new ShipPart(0, i));
            } else {
                shipBlocks.add(new ShipPart(i, 0));
            }
        }
    }

    public void setCoords(int row, int col) {
        for (ShipPart p : shipBlocks) {
            p.setRow(p.getRow() + row);
            p.setCol(p.getCol() + col);
        }
    }

    public List<ShipPart> getShipBlocks() {
        return shipBlocks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ShipPart p : shipBlocks) {
            sb.append(p.toString() + " ");
        }
        return "Ship{" + sb.toString() + "}";
    }
}
