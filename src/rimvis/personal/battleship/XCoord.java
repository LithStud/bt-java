package rimvis.personal.battleship;

public enum XCoord {
    R, E, S, P, U, B, L, I, K, A
}
