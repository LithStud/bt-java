package rimvis.personal.battleship;

public class Coords {
    private int col;
    private int row;

    public Coords(int col, int row) {
        this.col = col;
        this.row = row;
    }
}
