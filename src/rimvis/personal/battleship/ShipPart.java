package rimvis.personal.battleship;

import java.util.Vector;

public class ShipPart {
    private boolean alive;
    private int col;
    private int row;

    public ShipPart(int col, int row) {
        this.col = col;
        this.row = row;
    }

    @Override
    public String toString() {
        return "[" + col + "][" + row + "]";
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
