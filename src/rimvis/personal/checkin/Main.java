package rimvis.personal.checkin;

import com.sun.deploy.util.StringUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        /*try {
            // your first request that does the authentication
            URL authUrl = new URL("https://baltictalents.lt/checkin/login.php");
            HttpsURLConnection authCon = (HttpsURLConnection) authUrl.openConnection();
            authCon.connect();

            // temporary to build request cookie header
            StringBuilder sb = new StringBuilder();

            // find the cookies in the response header from the first request
            List<String> cookies = authCon.getHeaderFields().get("Set-Cookie");
            if (cookies != null) {
                for (String cookie : cookies) {
                    if (sb.length() > 0) {
                        sb.append("; ");
                    }

                    // only want the first part of the cookie header that has the value
                    String value = cookie.split(";")[0];
                    sb.append(value);
                }
            }

            // build request cookie header to send on all subsequent requests
            String cookieHeader = sb.toString();

            System.out.println(cookieHeader);

            // content
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(authCon.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            authCon.disconnect();
            Pattern pattern = Pattern.compile("name=\"token\" value=\"(.*?)\">");
            Matcher matcher = pattern.matcher(content);
            if (matcher.find())
            {
                System.out.println(matcher.group(1));
            }

            authCon.disconnect();

            // NEW CONNECTION

            URL regUrl = null;
            cookieHeader += "; token=" + matcher.group(1);

            regUrl = new URL("https://baltictalents.lt/checkin/login.php?email=lithstud%40gmail.com&password=mistika256");
            HttpsURLConnection regCon = (HttpsURLConnection) regUrl.openConnection();
            regCon.setRequestMethod("POST");
            regCon.setRequestProperty("Cookie", cookieHeader);
            regCon.setInstanceFollowRedirects(false);
            regCon.connect();

            //email=lithstud%40gmail.com&password=mistika256&token=1b678e6061e43b30810f512f7ff03ed3
            // login

            int status = regCon.getResponseCode();
            System.out.println(status);

            // content
            in = new BufferedReader(
                    new InputStreamReader(regCon.getInputStream()));
            content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            System.out.println(content);



            // with the cookie header your session should be preserved
           // URL regUrl = null;

            //regUrl = new URL("https://example.com/register");
            //HttpsURLConnection regCon = (HttpsURLConnection) regUrl.openConnection();
            //regCon.setRequestProperty("Cookie", cookieHeader);
            //regCon.connect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        URL url = null;
        try {
            url = new URL("https://baltictalents.lt/checkin/login.php");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            //cookies
            String cookiesHeader = con.getHeaderField("Set-Cookie");
            CookieManager cookieManager = new CookieManager();
            List<HttpCookie> cookies = HttpCookie.parse(cookiesHeader);
            String sendCookie = "";
            for (HttpCookie cookie : cookies) {
                cookieManager.getCookieStore().add(null, cookie);
                System.out.println(cookie.getName());
                System.out.println(cookie.getValue());
                sendCookie += cookie.getName() + "=" + cookie.getValue();
            }

            // status
            int status = con.getResponseCode();
            System.out.println(status);

            // content
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            Pattern pattern = Pattern.compile("name=\"token\" value=\"(.*?)\">");
            Matcher matcher = pattern.matcher(content);
            if (matcher.find())
            {
                System.out.println(matcher.group(1));
            }

            // lets try to checkout
            con = (HttpURLConnection) url.openConnection();

            con.setRequestProperty("Cookie", sendCookie);
            Map<String,String> arguments = new HashMap<>();
            arguments.put("email", "lithstud@gmail.com");
            arguments.put("password", "mistika256"); // This is a fake password obviously
            arguments.put("token", matcher.group(1)); // from previous request
            con.setRequestMethod("POST");
            con.setInstanceFollowRedirects(false);
            con.setDoOutput(true);
            StringJoiner sj = new StringJoiner("&");
            for(Map.Entry<String,String> entry : arguments.entrySet())
                sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
                        + URLEncoder.encode(entry.getValue(), "UTF-8"));
            byte[] out = sj.toString().getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            con.setFixedLengthStreamingMode(length);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            con.connect();

            //status = con.getResponseCode();
            //System.out.println(status);
            try(OutputStream os = con.getOutputStream()) {
                os.write(out);
            }

            int responseCode = con.getResponseCode();
            //System.out.println( responseCode );
            String location = con.getHeaderField( "Location" );
            //System.out.println( location );

            if (responseCode == 302 && location.equals("check.php")) {
                url = new URL("https://baltictalents.lt/checkin/check.php");
                con = (HttpURLConnection) url.openConnection();
                con.setRequestProperty("Cookie", sendCookie);
                con.setRequestMethod("GET");

                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                System.out.println(content);
            }

           /* cookiesHeader = con.getHeaderField("Set-Cookie");
            System.out.println(cookiesHeader);*/
            //cookieManager = new CookieManager();
            /*cookies = HttpCookie.parse(cookiesHeader);
            for (HttpCookie cookie : cookies) {
                cookieManager.getCookieStore().add(null, cookie);
                System.out.println(cookie.getName());
                System.out.println(cookie.getValue());
            }*/
             /*in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
             content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            System.out.println(content);*/
            // Do something with http.getInputStream()

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
